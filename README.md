# Dice Roll Game

Welcome to the Dice Roll Game! This project was created as part of a project assignment for the course 'Razvoj Mobilnih Aplikacija' (Mobile Application Development).

The Dice Roll Game is a simple, interactive game where players can roll a dice and compete to achieve the highest score. It serves as an educational tool to understand basic game mechanics, random number generation, and user interaction in a mobile environment, specifically designed for Android devices.


## Features

- Intuitive user interface suitable for Android devices
- User Authentication: Users can register and log in using their email and password.
- Dice Rolling Functionality: Users can roll a dice that generates a random number between 1 and 6, with results displayed immediately.
- Result Tracking: The app tracks the total number of rolls, the cumulative score, and the last roll result, all stored in a Firebase Firestore Database.
- History Screen: Users can view the history of their dice rolls, including the number of rolls and total score.
- Reset Functionality: Users can reset their dice roll history with the click of a button.
- Popular Board Games: The app displays the top 10 most popular board games using data retrieved from the BoardGameGeek API.
## How It's Made:

**Tech used:** Android Studio, Java/Kotlin, Jetpack Compose, Firebase Authentication, Firebase Database, BoardGameGeek API

## Documentation

[Documentation](https://gitlab.com/katarina6017/rma-projekt-dicerollgame/-/blob/main/Tehni%C4%8Dka_dokumentacija_-_RMA_Katarina_%C5%A0uljug.docx)

## App Preview

### Registration Screen
<div align="center">
   <img src="https://gitlab.com/katarina6017/rma-projekt-dicerollgame/-/raw/main/screenshots/reg_screen.jpg" alt="Registration Screen" width="300"/>
</div>
<br>

### Login Screen
<div align="center">
   <img src="https://gitlab.com/katarina6017/rma-projekt-dicerollgame/-/raw/main/screenshots/login_screen.jpg" alt="Login Screen" width="300"/>
</div>
<br>

### Main Screen
<div align="center">
   <img src="https://gitlab.com/katarina6017/rma-projekt-dicerollgame/-/raw/main/screenshots/main_screen.jpg" alt="Main Screen" width="300"/>
</div>
<br>

### History Screen
<div align="center">
   <img src="https://gitlab.com/katarina6017/rma-projekt-dicerollgame/-/raw/main/screenshots/history_screen.jpg" alt="History Screen" width="300"/>
</div>
