package com.nativemobilebits.loginflow.data.dice

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import java.util.concurrent.atomic.AtomicBoolean

data class UserScore(val userId: String, val totalScore: Int, val rollCount: Int)

class DiceViewModel : ViewModel() {
    private val _lastRoll = MutableStateFlow(0)
    val lastRoll: StateFlow<Int> = _lastRoll

    private val _totalScore = MutableStateFlow(0)
    val totalScore: StateFlow<Int> = _totalScore

    private val _rollCount = MutableStateFlow(0)
    val rollCount: StateFlow<Int> = _rollCount

    private val _topUsers = MutableStateFlow<List<UserScore>>(emptyList())
    val topUsers: StateFlow<List<UserScore>> = _topUsers

    private val firestore = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private var currentUser = auth.currentUser
    private val isInitialized = AtomicBoolean(false)

    init {
        observeAuthState()
        fetchCurrentScores()
    }

    fun rollDice() {
        val newRoll = (1..6).random()
        _lastRoll.update { newRoll }
        _totalScore.update { it + newRoll }
        _rollCount.update { it + 1 }
        saveRollResult(newRoll, _totalScore.value, _rollCount.value)
    }

    private fun saveRollResult(lastRoll: Int, totalScore: Int, rollCount: Int) {
        currentUser?.let {
            val uid = it.uid
            val result = mapOf(
                "lastRoll" to lastRoll,
                "totalScore" to totalScore,
                "rollCount" to rollCount
            )
            firestore.collection("users")
                .document(uid)
                .collection("rolls")
                .document("currentScore")
                .set(result)
                .addOnSuccessListener {
                    // Handle success if needed
                }
                .addOnFailureListener { e ->
                    Log.e("DiceViewModel", "Error saving roll result", e)
                }
        }
    }

    private fun fetchCurrentScores() {
        currentUser?.let {
            val uid = it.uid
            firestore.collection("users")
                .document(uid)
                .collection("rolls")
                .document("currentScore")
                .get()
                .addOnSuccessListener { document ->
                    if (document != null && document.exists()) {
                        val data = document.data
                        val lastRollValue = (data?.get("lastRoll") as? Long)?.toInt() ?: 0
                        val totalScoreValue = (data?.get("totalScore") as? Long)?.toInt() ?: 0
                        val rollCountValue = (data?.get("rollCount") as? Long)?.toInt() ?: 0
                        _lastRoll.update { lastRollValue }
                        _totalScore.update { totalScoreValue }
                        _rollCount.update { rollCountValue }
                    } else {
                        resetScores()
                    }
                }
                .addOnFailureListener { e ->
                    Log.e("DiceViewModel", "Error fetching current scores", e)
                    resetScores()
                }
        }
    }

    private fun resetScores() {
        _lastRoll.update { 0 }
        _totalScore.update { 0 }
        _rollCount.update { 0 }
    }

    fun resetGame() {
        resetScores()
        saveRollResult(0, 0, 0)
    }

    private fun observeAuthState() {
        auth.addAuthStateListener { auth ->
            val newUser = auth.currentUser
            if (currentUser?.uid != newUser?.uid) {
                currentUser = newUser
                if (newUser == null) {
                    resetScores()
                } else {
                    fetchCurrentScores()
                }
            }
        }
    }


}
