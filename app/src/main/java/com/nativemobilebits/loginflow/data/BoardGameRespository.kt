package com.nativemobilebits.loginflow.data
import com.nativemobilebits.loginflow.data.boardgame.BoardGame
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import org.w3c.dom.Element
import org.xml.sax.InputSource
import java.io.StringReader
import javax.xml.parsers.DocumentBuilderFactory

class BoardGameRepository {

    private val client = OkHttpClient()

    suspend fun fetchTopBoardGames(): List<BoardGame> = withContext(Dispatchers.IO) {
        val request = Request.Builder()
            .url("https://boardgamegeek.com/xmlapi2/hot?type=boardgame")
            .build()

        val response = client.newCall(request).execute()
        val xmlData = response.body?.string() ?: ""

        val factory = DocumentBuilderFactory.newInstance()
        val builder = factory.newDocumentBuilder()
        val inputSource = InputSource(StringReader(xmlData))
        val xmlDoc = builder.parse(inputSource)

        val items = xmlDoc.getElementsByTagName("item")
        val games = mutableListOf<BoardGame>()

        for (i in 0 until items.length) {
            val item = items.item(i) as Element
            val id = item.getAttribute("id")
            val name = item.getElementsByTagName("name").item(0).attributes.getNamedItem("value").nodeValue
            val rank = i + 1 // Pošto API ne vraća rang direktno, koristimo poziciju u listi

            games.add(BoardGame(id, name, rank))
        }

        return@withContext games.take(10) // Vraćamo samo top 10 igara
    }
}