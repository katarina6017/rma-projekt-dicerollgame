package com.nativemobilebits.loginflow.data.boardgame

data class BoardGame(
    val id: String,
    val name: String,
    val rank: Int
)