package com.nativemobilebits.loginflow.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.nativemobilebits.loginflow.data.BoardGameRepository
import com.nativemobilebits.loginflow.data.boardgame.BoardGame
import com.nativemobilebits.loginflow.navigation.PostOfficeAppRouter
import com.nativemobilebits.loginflow.navigation.Screen
import com.nativemobilebits.loginflow.ui.theme.LoginFlowTheme

@Composable
fun TopBoardGamesScreen() {
    val games = remember { mutableStateOf(listOf<BoardGame>()) }
    val repository = BoardGameRepository()

    LaunchedEffect(Unit) {
        games.value = repository.fetchTopBoardGames()
    }

    Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(text = "Top 10 Board Games Right Now", style = MaterialTheme.typography.headlineSmall)
                Spacer(modifier = Modifier.height(64.dp))
                LazyColumn(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    items(games.value) { game ->
                        Text(
                            text = "${game.rank}. ${game.name}",
                            style = MaterialTheme.typography.bodyLarge.copy(
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold
                            )
                        )
                    }
                }
                Spacer(modifier = Modifier.height(64.dp))
                Button(onClick = { PostOfficeAppRouter.navigateTo(Screen.HomeScreen) }) {
                    Text("Back to Dice")
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun TopBoardGamesScreenPreview() {
    LoginFlowTheme {
        TopBoardGamesScreen()
    }
}
