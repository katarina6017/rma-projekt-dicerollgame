package com.nativemobilebits.loginflow.screens

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.rememberScaffoldState
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.nativemobilebits.loginflow.components.AppToolbar
import com.nativemobilebits.loginflow.components.NavigationDrawerBody
import com.nativemobilebits.loginflow.components.NavigationDrawerHeader
import com.nativemobilebits.loginflow.data.dice.DiceViewModel
import com.nativemobilebits.loginflow.navigation.PostOfficeAppRouter
import com.nativemobilebits.loginflow.navigation.Screen
import kotlinx.coroutines.launch
import com.nativemobilebits.loginflow.R
import com.nativemobilebits.loginflow.screens.TopBoardGamesScreen

import com.nativemobilebits.loginflow.data.home.HomeViewModel

import androidx.compose.runtime.getValue


@Composable
fun HomeScreen(diceViewModel: DiceViewModel = viewModel(), homeViewModel: HomeViewModel = viewModel()) {
    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(homeViewModel) {
        homeViewModel.getUserData()
    }


    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            AppToolbar(toolbarTitle = stringResource(id = R.string.home),
                logoutButtonClicked = {
                    homeViewModel.logout()
                },
                navigationIconClicked = {
                    coroutineScope.launch {
                        scaffoldState.drawerState.open()
                    }
                }
            )
        },
        drawerGesturesEnabled = scaffoldState.drawerState.isOpen,
        drawerContent = {
            NavigationDrawerHeader(homeViewModel.emailId.value)
            NavigationDrawerBody(navigationDrawerItems = homeViewModel.navigationItemsList,
                onNavigationItemClicked = {
                    Log.d("ComingHere","inside_NavigationItemClicked")
                    Log.d("ComingHere","${it.itemId} ${it.title}")
                })

        }
    ) { paddingValues ->
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White)
                .padding(paddingValues)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
            ) {
                DiceRollerApp(
                    diceViewModel = diceViewModel,
                    modifier = Modifier
                        .fillMaxSize()
                        .wrapContentSize(Alignment.Center)
                )
            }
        }
    }
}

@Preview
@Composable
fun HomeScreenPreview() {
    HomeScreen()
}

@Composable
fun DiceRollerApp(diceViewModel: DiceViewModel, modifier: Modifier = Modifier) {
    DiceWithButtonAndImage(
        diceViewModel = diceViewModel,
        modifier = modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
    )
}

@Composable
fun DiceWithButtonAndImage(diceViewModel: DiceViewModel, modifier: Modifier = Modifier) {
    val lastRoll by diceViewModel.lastRoll.collectAsState()
    val totalScore by diceViewModel.totalScore.collectAsState()

    val imageResource = when (lastRoll) {
        1 -> R.drawable.dice_1
        2 -> R.drawable.dice_2
        3 -> R.drawable.dice_3
        4 -> R.drawable.dice_4
        5 -> R.drawable.dice_5
        else -> R.drawable.dice_6
    }

    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(imageResource),
            contentDescription = lastRoll.toString()
        )
        Spacer(modifier = Modifier.height(16.dp))
        Button(onClick = { diceViewModel.rollDice() }) {
            Text(stringResource(R.string.roll))
        }
        Spacer(modifier = Modifier.height(16.dp))
        Button(onClick = { PostOfficeAppRouter.navigateTo(Screen.HistoryScreen)}) {
            Text(stringResource(R.string.history))
        }
        Spacer(modifier = Modifier.height(16.dp))
        Button(onClick = { PostOfficeAppRouter.navigateTo(Screen.TopBoardGameScreen)}) {
            Text(stringResource(R.string.boardgame))
        }
    }
}
