package com.nativemobilebits.loginflow.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nativemobilebits.loginflow.R
import com.nativemobilebits.loginflow.components.HeadingTextComponent
import com.nativemobilebits.loginflow.data.dice.DiceViewModel
import com.nativemobilebits.loginflow.navigation.PostOfficeAppRouter
import com.nativemobilebits.loginflow.navigation.Screen

@Composable
fun HistoryScreen(diceViewModel: DiceViewModel = viewModel(), modifier: Modifier = Modifier) {
    val (history, setHistory) = remember { mutableStateOf(emptyList<Map<String, Int>>()) }
    val lastRoll by diceViewModel.lastRoll.collectAsState()
    val totalScore by diceViewModel.totalScore.collectAsState()
    val rollCount by diceViewModel.rollCount.collectAsState()

    LaunchedEffect(Unit) {
        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            val uid = it.uid
            val database = FirebaseDatabase.getInstance().getReference("users/$uid/rollResults")
            database.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val results = mutableListOf<Map<String, Int>>()
                    for (resultSnapshot in snapshot.children) {
                        val result = resultSnapshot.getValue(object : GenericTypeIndicator<Map<String, Int>>() {})
                        result?.let { results.add(it) }
                    }
                    setHistory(results)
                }

                override fun onCancelled(error: DatabaseError) {
                    // Handle database error
                }
            })
        }
    }

    Surface(modifier = Modifier
        .fillMaxSize()
        .background(color = Color.White)
        .padding(16.dp)) {

        HeadingTextComponent(value = stringResource(R.string.history))

        Column(
            modifier = modifier,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(32.dp))
            Text(text = "Current Last Roll: $lastRoll")
            Text(text = "Current Number of Rolls: $rollCount")
            Text(text = "Current Total Score: $totalScore")
            Spacer(modifier = Modifier.height(16.dp))
            history.forEach { result ->
                Text(text = "Last Roll: ${result["lastRoll"] ?: 0}")
                Text(text = "Total Score: ${result["totalScore"] ?: 0}")
                Spacer(modifier = Modifier.height(16.dp))
            }
            Spacer(modifier = Modifier.height(32.dp))
            Button(onClick = { diceViewModel.resetGame() }) { // Poziva funkciju za resetiranje
                Text("Reset")
            }
            Spacer(modifier = Modifier.height(32.dp))
            Button(onClick = { PostOfficeAppRouter.navigateTo(Screen.HomeScreen) }) {
                Text("Back to Dice")
            }
        }
    }
}

@Preview
@Composable
fun HistoryScreenPreview() {
    HistoryScreen()
}
